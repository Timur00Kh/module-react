import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { removeFromBasketAction } from "../store/basketStore"

export const useBasket = () => {
  const basket = useSelector((state) => state.basket)
  return basket
}

export const useBasketTotalPrice = () => {
  const basket = useSelector((state) => state.basket)
  const totalPrice = basket
    .map((product) => product.price)
    .reduce((sum, a) => sum + a, 0)
  return totalPrice
}

export const useBasketCount = () => {
    return useSelector((state) => state.basket.length)
}

export const useBasketTotalPriceFormat = () => {
    return useBasketTotalPrice().toLocaleString('ru')
}

export const useRemoveFromBasket = () => {
    const dispatch = useDispatch();
    return (id) => dispatch(removeFromBasketAction(id)) 
}

export function useGoBack() {
  const navigate = useNavigate()
  return () => {
    if(window.history.length > 1 && 
      document.referrer.indexOf(window.location.host) !== -1) {
        navigate(-1);
      }
      else {
        navigate('/');
      }
  }
}
