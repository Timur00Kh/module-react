import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import BasketPage from './pages/Basket/Basket'
import ProductPage from './pages/Product/Product'
import Home from './pages/Home/Home'
import { useEffect } from 'react'
import { data } from './_data'
import { useDispatch } from 'react-redux'
import { addProductListAction } from './store/productStore'
import { AuthPage } from './pages/Auth/Auth'



function App() {
  const dispatch = useDispatch()
  const addProductList = (list) => dispatch(addProductListAction(list))

  useEffect(() => {
    // TODO: data fetching
    addProductList(data);
  }, [])

  return (
    <Router>
        <Routes>
          <Route path="/auth" element={<AuthPage />} />
          <Route path="/product/:productId" element={<ProductPage />} />
          <Route path="/basket" element={<BasketPage />} />
          <Route path="/" element={<Home />} />
        </Routes>
    </Router>
  )
}

export default App


