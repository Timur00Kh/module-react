import { useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { BasketCard } from '../../components/BasketCard/BasketCard'
import Button from '../../components/Button/Button'
import BackButton from '../../components/CircleButton/BackButton'
import { useLogOut, useRedirectIfNotAuth } from '../../hooks/authHooks'
import {
  useBasket,
  useBasketTotalPriceFormat,
  useGoBack,
  useRemoveFromBasket,
} from '../../hooks/basketHooks'
import classes from './Basket.module.css'

export default function BasketPage() {
  const basket = useBasket()
  const removeFromBasket = useRemoveFromBasket()

  const goBack = useGoBack();

  useRedirectIfNotAuth()

  const totalPrice = useBasketTotalPriceFormat()
  const logOut = useLogOut()

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <BackButton onClick={() => goBack()} />
        <h1 className={classes.title}>Корзина с выбранными товарами</h1>
        <div className={classes.right}>
          <Button onClick={() => logOut()} outline>
            Выйти
          </Button>
        </div>
      </div>
      <main className={classes.main}>
        {basket.map((product) => (
          <div className={classes.item}>
            <BasketCard
              id={product.id}
              title={product.title}
              img={product.img}
              price={product.price}
              onRemoveClick={() => removeFromBasket(product.id)}
            />
          </div>
        ))}
      </main>

      <footer className={classes.footer}>
        <div className={classes.footer_inner}>
          <span className={classes.footer_text}>Заказ на сумму:</span>
          <span className={classes.footer_price}>{totalPrice} ₽</span>
          <div className={classes.right}>
            <Button onClick={() => alert('Заказ принят!')}>
              Оформить заказ
            </Button>
          </div>
        </div>
      </footer>
    </div>
  )
}
