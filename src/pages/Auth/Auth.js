import SignUp from '../../components/SignUp/signUp'
import classes from './Auth.module.css'

export function AuthPage() {
  return (
    <div className={classes.container}>
      <SignUp />
    </div>
  )
}
