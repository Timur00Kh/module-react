import {
  Link,
  useMatch,
  useNavigate,
  useParams,
  useRouteError,
} from 'react-router-dom'
import classes from './Product.module.css'
import { useSelector } from 'react-redux'
import BackButton from '../../components/CircleButton/BackButton'
import Button from '../../components/Button/Button'
import BasketButton from '../../components/CircleButton/BasketButton'
import {
  useBasketCount,
  useBasketTotalPriceFormat,
  useGoBack,
} from '../../hooks/basketHooks'
import { useRedirectIfNotAuth } from '../../hooks/authHooks'
import { BasketHeader } from '../../components/BasketHeader/BasketHeader'

export default function ProductPage() {
  const { productId } = useParams()
  const productList = useSelector((state) => state.product)
  const product = productList.find((e) => e.id === Number(productId))
  const goBack = useGoBack();



  useRedirectIfNotAuth()

  if (!product) {
    return ''
  }

  const price = product.price?.toLocaleString('ru')
  const amount = product.amount?.toLocaleString('ru')
  const weigth = product.weigth?.toLocaleString('ru')

  

  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <BackButton onClick={() => goBack()} />

        <div className={classes.header_right}>
          <BasketHeader/>
        </div>
      </div>
      <div className={classes.main}>
        <img className={classes.main_img} src={product.img} />

        <div className={classes.main_text}>
          <h1 className={classes.title}>{product.title}</h1>
          <span className={classes.text}>{product.text}</span>
          <div className={classes.buttons}>
            <div>
              <span className={classes.price}>{price} ₽</span>
              <span className={classes.price_divider}>/</span>
              {Boolean(amount) && (
                <span className={classes.amount}>{amount} шт.</span>
              )}
              {Boolean(weigth) && (
                <span className={classes.amount}>{weigth} г.</span>
              )}
            </div>
            <div className={classes.basket_btn_wrapper}>
              <Button>В корзину</Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
