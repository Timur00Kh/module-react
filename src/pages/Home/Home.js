import Card from '../../components/Card/Card'
import { data } from '../../_data'
import KorzinaButton from '../../components/CircleButton/BasketButton'
import classes from './Home.module.css'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { addToBasketAction } from '../../store/basketStore'
import { useBasket, useBasketTotalPrice } from '../../hooks/basketHooks'
import { useRedirectIfNotAuth } from '../../hooks/authHooks'
import { BasketHeader } from '../../components/BasketHeader/BasketHeader'

export default function Home() {
  const basket = useBasket()
  const dispatch = useDispatch()

  const addToBasket = (product) => dispatch(addToBasketAction(product))

  useRedirectIfNotAuth()

  const totalPrice = useBasketTotalPrice()

  return (
    <>
      <header className={classes.header}>
        <h1 className={classes.header_text}>Наша продукция</h1>
        <div style={{ color: 'white' }}>
          <BasketHeader />
        </div>
      </header>
      <div className="cards">
        {data.map((product) => {
          const { id, img, title, text } = product
          const price = product.price?.toLocaleString('ru')
          const amount = product.amount?.toLocaleString('ru')
          const weigth = product.weigth?.toLocaleString('ru')
          return (
            <div key={id} className="cards__wrapper">
              <Card
                title={title}
                img={img}
                text={text}
                price={price}
                amount={amount}
                weigth={weigth}
                onButtonClick={() => addToBasket(product)}
                url={`/product/${id}`}
              />
            </div>
          )
        })}
      </div>
    </>
  )
}
