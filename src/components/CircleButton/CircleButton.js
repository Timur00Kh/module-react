import classes from './CircleButton.module.css'

export default function CircleButton({ onClick, size = 30, children, className }) {
  return (
    <button
      className={classes.button + ' ' + className}
      onClick={onClick}
      style={{ width: size, height: size }}
    >
      {children}
    </button>
  )
}
