import classes from './Card.module.css'
import CardButton from '../CircleButton/CardButton'
import { Link } from 'react-router-dom'

export default function Card({
  img,
  title,
  text,
  price,
  amount,
  weigth,
  onButtonClick,
  url,
}) {
  return (
    <div className={classes.card}>
      <Link to={url}>
        <img className={classes.img} src={img} alt={title} />
      </Link>
      <span className={classes.title}>{title}</span>
      <span className={classes.text}>{text}</span>
      <div className={classes.bottom}>
        <div>
          <span className={classes.price}>{price} ₽</span>
          <span className={classes.divider}>/</span>
          {Boolean(amount) && (
            <span className={classes.amount}>{amount} шт.</span>
          )}
          {Boolean(weigth) && (
            <span className={classes.amount}>{weigth} г.</span>
          )}
        </div>
        <CardButton onClick={onButtonClick} />
      </div>
    </div>
  )
}
