const requiredError = 'Поле обязательно для заполнения';

export function getPasswordError(password) {
    if (password.length <= 0) {
        return requiredError;
    }
    if (password.length < 8) {
        return 'Пароль должен содержать как минимум 8 символов';
    }
    return '';
}

export function getEmailError(email) {
    if (email.length <= 0) {
        return requiredError;
    }

    const re =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = re.test(String(email).toLowerCase());
    
    if (!isValid) {
        return 'Email невалидный';
    }
    return '';
}

export function getCheckboxError(value) {
    if (!value) {
        return requiredError;
    }
    return '';
}
