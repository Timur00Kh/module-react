import React, { useState, useMemo, useEffect } from 'react'
import './signUp.css'
import { getCheckboxError, getEmailError, getPasswordError } from './validators'
import classnames from 'classnames'
import { useDispatch } from 'react-redux'
import { signInAction } from '../../store/authStore'
import { useNavigate } from 'react-router-dom'

const errorClass = 'fieldset_error'
const USER_DB_LOCAL_STORAGE_NAME = 'USER_DB_LOCAL_STORAGE_NAME'

function SignUp() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [checkbox, setCheckbox] = useState(false)
  const [showError, setShowError] = useState(false)

  const dispatch = useDispatch()
  const signIn = (auth) => dispatch(signInAction(auth))
  const navigate = useNavigate()

  const emailError = useMemo(() => getEmailError(email), [email])
  const passwordError = useMemo(() => getPasswordError(password), [password])
  const checkboxError = useMemo(() => getCheckboxError(checkbox), [checkbox])

  const [isSignUp, setIsSignUp] = useState(false)

  const onClick = () => {
    setShowError(true)

    const isValid = ![
      emailError,
      passwordError,
      isSignUp ? checkboxError : '',
    ].some((err) => err !== '')

    if (!isValid) {
      return
    }

    const userDBstring = localStorage.getItem(USER_DB_LOCAL_STORAGE_NAME)
    const userDB = userDBstring ? JSON.parse(userDBstring) : []
    const currentUser = userDB.find((user) => user.email === email)

    if (isSignUp) {
      if (currentUser) {
        // TODO: throw error
        console.error('Пользователь с таким email уже существует')
        return
      }

      localStorage.setItem(
        USER_DB_LOCAL_STORAGE_NAME,
        JSON.stringify([...userDB, { email, password }])
      )

      setIsSignUp(false)
    } else {
      if (!currentUser || currentUser.password !== password) {
        // TODO: throw error
        console.error('Логин или пароль неверный')
        return
      }

      signIn({ email, password })
      navigate('/')
    }

    setEmail('')
    setPassword('')
    setCheckbox(false)
    setShowError(false)
  }

  useEffect(() => {
    const handler = ({ key }) => {
      if (key === 'Enter') {
        onClick()
      }
    }

    window.addEventListener('keypress', handler)

    return () => {
      window.removeEventListener('keypress', handler)
    }
  }, [onClick])

  return (
    <div className="sign-up" noValidate>
      <div className="sign-up_link-container">
        <button onClick={() => setIsSignUp(!isSignUp)} className="sign-up_link">
          {isSignUp ? 'Авторизация' : 'Регистрация'}
        </button>
      </div>
      <h1 className="sign-up__header">
        {isSignUp ? 'Регистрация' : 'Авторизация'}
      </h1>
      <fieldset
        className={classnames('sign-up__fieldset fieldset', {
          [errorClass]: showError && emailError,
        })}
      >
        <label className="fieldset__label" htmlFor="email">
          Email
        </label>
        <input
          value={email}
          onChange={(e) => setEmail(e.currentTarget.value)}
          className="fieldset__input"
          type="email"
          name="email"
          placeholder="Введите email"
        />
        <span className="fieldset__error">{emailError}</span>
      </fieldset>
      <fieldset
        className={classnames('sign-up__fieldset fieldset', {
          [errorClass]: showError && passwordError,
        })}
      >
        <label className="fieldset__label" htmlFor="password">
          Пароль
        </label>
        <input
          value={password}
          onChange={(e) => setPassword(e.currentTarget.value)}
          className="fieldset__input"
          type="password"
          name="password"
          placeholder="Введите пароль"
        />
        <span className="fieldset__error">{passwordError}</span>
      </fieldset>

      {isSignUp && (
        <fieldset
          className={classnames('sign-up__fieldset fieldset checkbox', {
            [errorClass]: showError && checkboxError,
          })}
        >
          <label className="checkbox__label" htmlFor="checkbox">
            <input
              checked={checkbox}
              onChange={(e) => setCheckbox(e.currentTarget.checked)}
              type="checkbox"
            />
            <span className="checkbox__text">
              Я согласен с
              <a className="checkbox__text-link" href="#/asdasd">
                {' '}
                Правилами пользования приложением{' '}
              </a>
            </span>
          </label>
          <span className="fieldset__error">{checkboxError}</span>
        </fieldset>
      )}
      <fieldset className="sign-up__fieldset submit">
        <button onClick={onClick} className="submit__button">
          {isSignUp ? 'Регистрация' : 'Авторизация'}
        </button>
      </fieldset>
    </div>
  )
}

export default SignUp
