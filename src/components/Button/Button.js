import classes from './Button.module.css'
import classNames from 'classnames'

export default function Button({ children, outline, onClick }) {
  return (
    <button
      onClick={onClick}
      className={classNames(classes.btn, {
        [classes.fill]: !outline,
      })}
    >
      {children}
    </button>
  )
}
