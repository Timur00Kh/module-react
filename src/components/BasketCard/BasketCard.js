import Button from '../Button/Button'
import RemoveButton from '../CircleButton/RemoveButton'
import classes from './BasketCard.module.css'

export function BasketCard({ id, img, title, price, onRemoveClick }) {
  const formatPrice = price.toLocaleString('ru')

  return (
    <div className={classes.container}>
      <img className={classes.img} src={img} />
      <span className={classes.title}>{title}</span>
      <div className={classes.right}>
        <span className={classes.price}>{formatPrice} ₽</span>
        <RemoveButton onClick={onRemoveClick}/>
      </div>
    </div>
  )
}
