import { useState } from 'react';
import './Counter.css'

function Counter() {
    const [count, setCount] = useState(0);


  return (
    <>
      <h1 className="counter__header">Кнопка нажата: {count} раз</h1>
      <button 
        className='counter__btn' 
        onClick={() => setCount(count + 1)}
        >
          Клик
      </button>

      <div className='counter__item'>
        Счетчик больше 10: {count > 10 ? 'Да' : 'Нет'}
      </div> 
      <div className='counter__item'>
        Счетчик больше 20: {count > 20 ? 'Да' : 'Нет'}  
      </div> 
    </>
  );
}

export default Counter;