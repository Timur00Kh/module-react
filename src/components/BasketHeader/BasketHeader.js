import { Link } from "react-router-dom"
import { useLogOut } from "../../hooks/authHooks";
import { useBasketCount, useBasketTotalPriceFormat } from "../../hooks/basketHooks"
import Button from "../Button/Button"
import BasketButton from "../CircleButton/BasketButton"
import classes from './BasketHeader.module.css';

export function BasketHeader() {



    const totalPrice = useBasketTotalPriceFormat()
    const basketCount = useBasketCount()
    const logOut = useLogOut();

    return <div className={classes.header_right}>
    <div className={classes.counter}>
      {basketCount} товара <br /> на сумму {totalPrice} ₽
    </div>
    <Link to="/basket">
      <BasketButton />
    </Link>
    <Button onClick={logOut} outline>Выйти</Button>
  </div>
}